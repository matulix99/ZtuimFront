package app.plants.retrofit;

import com.google.gson.Gson;

import retrofit2.converter.gson.GsonConverterFactory;

public class Retrofit {
    private retrofit2.Retrofit retrofit;

    public Retrofit() {
        initializeRetrofit();
    }

    public retrofit2.Retrofit getRetrofit() {
        return retrofit;
    }

    private void initializeRetrofit() {

        retrofit = new retrofit2.Retrofit.Builder().baseUrl("http://172.28.122.58:8080")
                .addConverterFactory(GsonConverterFactory.create(new Gson())).build();
    }
}
