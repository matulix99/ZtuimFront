package app.plants;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.plants.api.PlantApi;
import app.plants.model.Plant;
import app.plants.retrofit.Retrofit;
import app.plants.view.CourseAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.rv_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        Button add = findViewById(R.id.rv_save);

        Retrofit retrofit = new Retrofit();
        PlantApi plantApi = retrofit.getRetrofit().create(PlantApi.class);
        plantApi.getPlants().enqueue(new Callback<List<Plant>>() {

            @Override
            public void onResponse(@NonNull Call<List<Plant>> call, @NonNull Response<List<Plant>> response) {
                CourseAdapter plantAdapter = new CourseAdapter(response.body(), plant -> {
                    Toast.makeText(MainActivity.this, plant.getId(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(MainActivity.this, PlantsEdit.class);
                    intent.putExtra("plantId", plant.getId());
                    startActivity(intent);
                });
                recyclerView.setAdapter(plantAdapter);
            }

            @Override
            public void onFailure(@NonNull Call<List<Plant>> call, @NonNull Throwable t) {
            }
        });

        add.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, PlantAdd.class));
            }
        });
    }
}