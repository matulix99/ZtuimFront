package app.plants.api;

import java.util.List;

import app.plants.model.Plant;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PlantApi {

    @GET("plant/allPlants")
    Call<List<Plant>> getPlants();

    @POST("/plant/save")
    Call<Plant> save(@Body Plant plant);

    @GET("/plant/getPlantById/{id}")
    Call<Plant> findById(@Path("id") long id);

    @PUT("/plant/updateById/{id}")
    Call<Plant> update(@Body Plant plant, @Path("id") long id);

    @DELETE("/plant/deleteById/{id}")
    Call<Void> delete(@Path("id") long id);
}
