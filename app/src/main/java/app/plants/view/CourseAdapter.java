package app.plants.view;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.plants.PlantsEdit;
import app.plants.R;
import app.plants.model.Plant;

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.Viewholder> {

    private final List<Plant> courseModelArrayList;

    public CourseAdapter(List<Plant> courseModelArrayList, ItemClickListener itemClickListener) {
        this.courseModelArrayList = courseModelArrayList;
    }

    @NonNull
    @Override
    public CourseAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CourseAdapter.Viewholder holder, int position) {
        Plant model = courseModelArrayList.get(position);
        holder.plantName.setText(model.getName());
        holder.plantingDate.setText(model.getPlantingDate());
        holder.floweringTime.setText(model.getFloweringTime());
        holder.maturityTime.setText(model.getMaturityTime());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), PlantsEdit.class);
                i.putExtra("id", model.getId());
                view.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return courseModelArrayList.size();
    }

    public interface ItemClickListener {

        void onItemClick(Plant plant);
    }

    public static class Viewholder extends RecyclerView.ViewHolder {

        private final TextView plantName;
        private final TextView plantingDate;
        private final TextView floweringTime;
        private final TextView maturityTime;
        public View view;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            plantName = itemView.findViewById(R.id.itemTv_plantName);
            plantingDate = itemView.findViewById(R.id.itemTv_plantingDate);
            floweringTime = itemView.findViewById(R.id.itemTv_floweringTime);
            maturityTime = itemView.findViewById(R.id.itemTv_maturityTime);
        }
    }
}
