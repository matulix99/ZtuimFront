package app.plants.view;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import app.plants.R;

public class ViewHolder extends RecyclerView.ViewHolder{

    public View view;
    public CardView cardView;
    public LinearLayout.LayoutParams layoutParams;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        view = itemView;
        layoutParams = new LinearLayout.LayoutParams(0,0);
        cardView = itemView.findViewById(R.id.cardview);
    }

    public void setPlantName(String id) {
        TextView tv_id = view.findViewById(R.id.itemTv_plantName);
        tv_id.setText(id);
    }

    public void setFloweringTime(String user) {
        TextView tv_user = view.findViewById(R.id.itemTv_floweringTime);
        tv_user.setText(user);
    }

    public void setPlantingDate(String status) {
        TextView tv_statusService = view.findViewById(R.id.itemTv_plantingDate);
        tv_statusService.setText(status);
    }

    public void setMaturityTime(String status) {
        TextView tv_statusExpert = view.findViewById(R.id.itemTv_maturityTime);
        tv_statusExpert.setText(status);
    }

    public void setBackColor(int color) {
        cardView.setBackgroundColor(color);
    }
}
