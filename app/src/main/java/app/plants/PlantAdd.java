package app.plants;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import app.plants.api.PlantApi;
import app.plants.model.Plant;
import app.plants.retrofit.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlantAdd extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_add);

        TextInputEditText plantName = findViewById(R.id.peEt_plantName1);
        TextInputEditText plantingDate = findViewById(R.id.peEt_plantingDate1);
        TextInputEditText floweringTime = findViewById(R.id.peEt_floweringTime1);
        TextInputEditText maturityTime = findViewById(R.id.peEt_maturityTime1);

        Retrofit retrofit = new Retrofit();
        PlantApi plantApi = retrofit.getRetrofit().create(PlantApi.class);

        Button save = findViewById(R.id.peEt_buttonSave);

        Plant plant = new Plant();

        save.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                plant.setName(Objects.requireNonNull(plantName.getText()).toString());
                plant.setPlantingDate(Objects.requireNonNull(plantingDate.getText()).toString());
                plant.setFloweringTime(Objects.requireNonNull(floweringTime.getText()).toString());
                plant.setMaturityTime(Objects.requireNonNull(maturityTime.getText()).toString());
                plantApi.save(plant).enqueue(new Callback<Plant>() {

                    @Override
                    public void onResponse(@NonNull Call<Plant> call, @NonNull Response<Plant> response) {
                        Toast.makeText(PlantAdd.this, "Save successful!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(PlantAdd.this, MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(@NonNull Call<Plant> call, @NonNull Throwable t) {

                    }
                });
            }
        });
    }
}