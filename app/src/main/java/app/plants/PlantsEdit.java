package app.plants;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import app.plants.api.PlantApi;
import app.plants.model.Plant;
import app.plants.retrofit.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlantsEdit extends AppCompatActivity {

    public Plant plant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitvity_plantsedit);

        TextInputEditText plantName = findViewById(R.id.peEt_plantName);
        TextInputEditText plantingDate = findViewById(R.id.peEt_plantingDate);
        TextInputEditText floweringTime = findViewById(R.id.peEt_floweringTime);
        TextInputEditText maturityTime = findViewById(R.id.peEt_maturityTime);

        Button update = findViewById(R.id.peEt_buttonUpdate);
        Button delete = findViewById(R.id.peEt_buttonDelete);

        Bundle extras = getIntent().getExtras();
        int strId = extras.getInt("id");
        System.out.println(strId);

        Retrofit retrofit = new Retrofit();
        PlantApi plantApi = retrofit.getRetrofit().create(PlantApi.class);

        plantApi.findById((long) strId).enqueue(new Callback() {
            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                plant = new Plant();
                plant = (Plant) response.body();
                assert plant != null;
                Toast.makeText(PlantsEdit.this, plant.getName(), Toast.LENGTH_LONG).show();
                plantName.setText(plant.getName());
                plantingDate.setText(plant.getPlantingDate());
                floweringTime.setText(plant.getFloweringTime());
                maturityTime.setText(plant.getMaturityTime());
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {

            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                plant.setName(Objects.requireNonNull(plantName.getText()).toString());
                plant.setPlantingDate(Objects.requireNonNull(plantingDate.getText()).toString());
                plant.setFloweringTime(Objects.requireNonNull(floweringTime.getText()).toString());
                plant.setMaturityTime(Objects.requireNonNull(maturityTime.getText()).toString());

                plantApi.update(plant, plant.getId()).enqueue(new Callback<Plant>() {
                    @Override
                    public void onResponse(@NonNull Call<Plant> call, @NonNull Response<Plant> response) {
                        Toast.makeText(PlantsEdit.this, "Update successful!", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(PlantsEdit.this, MainActivity.class));
                    }

                    @Override
                    public void onFailure(@NonNull Call<Plant> call, @NonNull Throwable t) {

                    }
                });
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                plantApi.delete(plant.getId()).enqueue(new Callback() {

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) {
                        Toast.makeText(PlantsEdit.this, "Delete successful!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(PlantsEdit.this, MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(@NonNull Call call, @NonNull Throwable t) {

                    }
                });
            }
        });
    }
}
